ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_bwa:0.7.17 AS opt_bwa

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=opt_bwa /opt/bin/ /opt/bin/
COPY --from=opt_bwa /opt/lib/ /opt/lib/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

ENTRYPOINT [ "/opt/bin/bwa" ]